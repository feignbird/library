from django.urls import path, include
from .views import BooksView, AuthorsView, PhoneNumberView
# , OneBookToManyAuthorView
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'books', BooksView)
router.register(r'authors', AuthorsView)
router.register(r'phone_number', PhoneNumberView)
# router.register(r'booktoauthor', OneBookToManyAuthorView)
# router.register('book_author', BookAuthorView)

urlpatterns = [
    # path('books', BooksView.as_view({'get':'list', 'post':'create'})),
    path('', include(router.urls))
    # path('booklist/', BooksList, name='books-list'),
    # path('bookDetail/<int:pk>/', BooksDetail, name='books-detail'),
    # path('authorlist/', AuthorsList, name='authors-list'),
    # path('authorDetail/<int:pk>/', AuthorsDetail, name='authors-detail'),
]