from multiprocessing import managers
from rest_framework import serializers
from .models import Books, Authors, PhoneNumber
import pytz


class PhoneNumberSerializer(serializers.HyperlinkedModelSerializer):
    author_number = serializers.PrimaryKeyRelatedField(required = False, read_only = True, many=True)
    # author_number = serializers.SlugRelatedField(slug_field='author_name', read_only = True)
    class Meta:
        model = PhoneNumber
        fields = ('id', 'url', 'phone_number', 'author_number')

class BookSerializer(serializers.HyperlinkedModelSerializer):
    # url = serializers.HyperlinkedIdentityField(view_name='books-detail', lookup_field='pk')
    # publish_time = serializers.DateTimeField()
    
    # def to_representation(self, instance):
    #     return super().to_representation(instance)
    
    publish_time = serializers.DateTimeField(format="%d | %B | %Y, %H:%M:%S", input_formats=["%d/%m/%Y, %H:%M:%S", "%dth, %B, %Y", "%d/%m/%Y"])

    class Meta:
        model = Books
        fields = ('id', 'book_name', 'publish_time')
    
    


class AuthorSerializer(serializers.HyperlinkedModelSerializer):
    # author_name = PhoneNumberSerializer(many = True)
    # url = serializers.HyperlinkedIdentityField(view_name='authors-detail', lookup_field='pk')
    # book_name = BookSerializer(many=True)
    # book_name = serializers.ListSerializer(child = BookSerializer())
    # book_name = serializers.ManyRelatedField(child_relation=BookSerializer())
    # book_name = serializers.SlugRelatedField(queryset=Books.objects.all(),  slug_field='id', many=True)
    # author_name = serializers.PrimaryKeyRelatedField(queryset = PhoneNumber.objects.all(), many=True)
    book_name = serializers.PrimaryKeyRelatedField(queryset = Books.objects.all(), many=True)
    # author_number = PhoneNumberSerializer(many=True, read_only=True)
    # book_name = serializers.HyperlinkedRelatedField(queryset = Books.objects.all(), view_name='books-detail', many = True)
    author_number = serializers.PrimaryKeyRelatedField(queryset = PhoneNumber.objects.all(), many = True)
    class Meta:
        model = Authors
        fields = ('id', 'url', 'author_name', 'book_name', 'author_number')
    
    # def create(self, validation_data):
    #     book_name_data = validation_data.pop('book_name')
    #     authors = Authors.objects.create(**validation_data)
    #     for books in book_name_data:
    #         Books.objects.create(authors = authors, **books)
    #     return authors.set()


class OneBookToManyAuthorSerializer(serializers.HyperlinkedModelSerializer):
    # authors = serializers.HyperlinkedRelatedField(queryset = Authors.objects.all(), view_name='books-detail')
    # authors = serializers.SlugRelatedField(queryset = Authors.objects.all(), slug_field='author_name', many=True)
    authors = serializers.HyperlinkedRelatedField(view_name='authors-detail', queryset = Authors.objects.all(), many = True)
    # error 'ManyRelatedManager' object has no attribure 'pk' comes due to our field does not have 'many = True' kwarg
    class Meta:
        model = Books
        fields = ('id', 'url', 'book_name', 'authors')





# class book_to_author_relation:
#     pass

# class BookAuthorSerializer(serializers.HyperlinkedModelSerializer):
#     book_name = serializers.CharField()
#     # author_name = serializers.SlugRelatedField(queryset = Authors.objects.all(), slug_field='author_name', many=True)
#     class Meta:
#         model = BookAuthor
#         fields = '__all__'
