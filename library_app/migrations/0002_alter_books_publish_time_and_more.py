# Generated by Django 4.0.1 on 2022-01-30 16:30

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('library_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='books',
            name='publish_time',
            field=models.DateTimeField(default=datetime.datetime(2022, 1, 30, 16, 30, 36, 332669, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='phonenumber',
            name='author_number',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='author_number', to='library_app.authors'),
        ),
    ]
