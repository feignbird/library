# from rest_framework.parsers import JSONParser
# from django.http import HttpResponse, JsonResponse
from django.contrib.auth.models import User
from rest_framework.decorators import action
from rest_framework.response import Response    
from rest_framework import viewsets, status
from .models import Books, Authors, PhoneNumber
from .serializers import AuthorSerializer, BookSerializer, OneBookToManyAuthorSerializer, PhoneNumberSerializer
from rest_framework import permissions
# from django.views.decorators.csrf import csrf_exempt


class BooksView(viewsets.ModelViewSet):
    queryset = Books.objects.all()
    # serializer_class = BookSerializer

    def get_serializer_class(self):
        if self.action == 'list':
            return OneBookToManyAuthorSerializer
        # return super().get_serializer_class()
        return BookSerializer

class AuthorsView(viewsets.ModelViewSet):
    queryset = Authors.objects.all()
    serializer_class = AuthorSerializer

class PhoneNumberView(viewsets.ModelViewSet):
    queryset = PhoneNumber.objects.all()
    serializer_class = PhoneNumberSerializer












# class OneBookToManyAuthorView(viewsets.ReadOnlyModelViewSet):
#     queryset = Books.objects.all()
#     serializer_class = OneBookToManyAuthorSerializer

# class BookAuthorView(viewsets.ModelViewSet):
#     queryset = BookAuthor.objects.all()
#     serializer_class = BookAuthorSerializer


# @csrf_exempt
# def BooksList(request):
#     if request.method == 'GET':
#         book_obj = Books.objects.all()
#         book_ser = BookSerializer(book_obj, context={'request':request})
#         return JsonResponse(book_ser.data, status=200)
#     elif request.method == 'POST':
#         data = JSONParser().parse(request)
#         book_ser = BookSerializer(data = data, context={'request':request})
#         if book_ser.is_valid():
#             book_ser.save()
#             return JsonResponse(book_ser.data, status = 201)
#         return HttpResponse(book_ser.errors, status = 404)


# @csrf_exempt
# def BooksDetail(request, pk):
#     try:
#         book_obj = Books.objects.get(pk=pk)
#     except Books.DoesNotExist:
#         return HttpResponse(status = 404)
#     if request.method == 'GET':
#         book_ser = BookSerializer(book_obj, context={'request':request})
#         return JsonResponse(book_ser, status = 201)
#     elif request.method == 'PUT':
#         data = JSONParser().parse(request)
#         book_ser = BookSerializer(data = data, context={'request':request})
#         if book_ser.is_valid():
#             book_ser.save()
#             return JsonResponse(book_ser.data, status=201)
#         return HttpResponse(book_ser.errors, status=404)
#     elif request.method == 'DELETE':
#         book_obj.delete()
#         return HttpResponse(status = 301)


# @csrf_exempt
# def AuthorsList(request):
#     if request.method == 'GET':
#         author_obj = Authors.objects.all()
#         author_ser = AuthorSerializer(author_obj, context={'request':request})
#         return JsonResponse(author_ser.data, status= 200)
#     elif request.method == 'POST':
#         data = JSONParser().parse(request)
#         author_ser = AuthorSerializer(data = data, context={'request':request})
#         if author_ser.is_valid():
#             author_ser.save()
#             return JsonResponse(author_ser.data, status=201)
#         return HttpResponse(author_ser.errors, status = 404)


# @csrf_exempt
# def AuthorsDetail(request, pk):
#     try:
#         author_obj = Authors.objects.get(pk = pk)
#     except Authors.DoesNotExist:
#         return HttpResponse(status = 404)
#     if request.method == 'GET':
#         author_ser = AuthorSerializer(author_obj, context={'request':request})
#         return JsonResponse(author_ser.data, status = 200)
#     elif request.method == 'PUT':
#         data = JSONParser().parse(request)
#         author_ser = AuthorSerializer(data = data, context={'request':request})
#         return JsonResponse(author_ser.data, status=201)
#     elif request.method == 'DELETE':
#         author_obj.delete()
#         return HttpResponse(status = 404)
