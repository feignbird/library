from django.db import models
from django.utils import timezone
# import datetime
# from django.utils import timezone
# Create your models here.
class Books(models.Model):
    book_name = models.CharField(max_length=255)
    publish_time = models.DateTimeField(default=timezone.now())


    def __str__(self):
        return "%s @ %s"%(self.book_name, self.publish_time)
    class Meta:
        ordering = ('publish_time',)

class Authors(models.Model):
    author_name = models.CharField(max_length=255)
    book_name = models.ManyToManyField(Books, related_name="authors")

    def __str__(self):
        return "%s @ %s"%(self.author_name, self.book_name)


class PhoneNumber(models.Model):
    phone_number = models.IntegerField()
    author_number = models.ForeignKey(Authors, related_name = 'author_number', on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.phone_number

